import numpy as np
import h5py as hp

np.set_printoptions(threshold=np.nan, linewidth=180)

def euler(U, V, W, P, T, hx, hy, hz):
    global dt, Pr, Ra
    global N, M, L

    Hx = np.zeros([N+1, M+1, L])
    Hx[1:N, 1:M, 1:L-1] = ((DDXi(U, N, M, L-1, hx) + DDEt(U, N, M, L-1, hy) + DDZt(U, N, M, L-1, hz))*0.5*Pr -
                            D_Xi(U, N, M, L-1, hx)*U[1:N, 1:M, 1:L-1] -
                      0.25*(V[1:N, 0:M-1, 1:L-1] + V[1:N, 1:M, 1:L-1] + V[1:N, 1:M, 2:L] + V[1:N, 0:M-1, 2:L])*D_Et(U, N, M, L-1, hy) - 
                      0.25*(W[0:N-1, 1:M, 1:L-1] + W[1:N, 1:M, 1:L-1] + W[1:N, 1:M, 2:L] + W[0:N-1, 1:M, 2:L])*D_Zt(U, N, M, L-1, hz))

    Hy = np.zeros([N+1, M, L+1])
    Hy[1:N, 1:M-1, 1:L] = ((DDXi(V, N, M-1, L, hx) + DDEt(V, N, M-1, L, hy) + DDZt(V, N, M-1, L, hz))*0.5*Pr -
                            D_Et(V, N, M-1, L, hy)*V[1:N, 1:M-1, 1:L] -
                      0.25*(U[1:N, 1:M-1, 0:L-1] + U[1:N, 1:M-1, 1:L] + U[1:N, 2:M, 1:L] + U[1:N, 2:M, 0:L-1])*D_Xi(V, N, M-1, L, hx) -
                      0.25*(W[0:N-1, 1:M-1, 1:L] + W[1:N, 1:M-1, 1:L] + W[1:N, 2:M, 1:L] + W[0:N-1, 2:M, 1:L])*D_Zt(V, N, M-1, L, hz))

    Hz = np.zeros([N, M+1, L+1])
    Hz[1:N-1, 1:M, 1:L] = ((DDXi(W, N-1, M, L, hx) + DDEt(W, N-1, M, L, hy) + DDZt(W, N-1, M, L, hz))*0.5*Pr -
                            D_Zt(W, N-1, M, L, hz)*W[1:N-1, 1:M, 1:L] -
                      0.25*(U[1:N-1, 1:M, 0:L-1] + U[1:N-1, 1:M, 1:L] + U[2:N, 1:M, 1:L] + U[2:N, 1:M, 0:L-1])*D_Xi(W, N-1, M, L, hx) -
                      0.25*(V[1:N-1, 0:M-1, 1:L] + V[1:N-1, 1:M, 1:L] + V[2:N, 1:M, 1:L] + V[2:N, 0:M-1, 1:L])*D_Et(W, N-1, M, L, hy) +
                      0.50*(T[2:N, 1:M, 1:L] + T[1:N-1, 1:M, 1:L])*Ra*Pr)

    # Calculating guessed values of U implicitly
    Hx[1:N, 1:M, 1:L-1] = U[1:N, 1:M, 1:L-1] + dt*(Hx[1:N, 1:M, 1:L-1] - (P[1:N, 1:M, 2:L] - P[1:N, 1:M, 1:L-1])/hx)
    Up = uJacobi(Hx, hx, hy, hz)

    # Calculating guessed values of V implicitly
    Hy[1:N, 1:M-1, 1:L] = V[1:N, 1:M-1, 1:L] + dt*(Hy[1:N, 1:M-1, 1:L] - (P[1:N, 2:M, 1:L] - P[1:N, 1:M-1, 1:L])/hy)
    Vp = vJacobi(Hy, hx, hy, hz)

    # Calculating guessed values of W implicitly
    Hz[1:N-1, 1:M, 1:L] = W[1:N-1, 1:M, 1:L] + dt*(Hz[1:N-1, 1:M, 1:L] - (P[2:N, 1:M, 1:L] - P[1:N-1, 1:M, 1:L])/hz)
    Wp = wJacobi(Hz, hx, hy, hz)

    # Calculating pressure correction term
    rhs = np.zeros([N+1, M+1, L+1])
    rhs[1:N, 1:M, 1:L] = ((Up[1:N, 1:M, 1:L] - Up[1:N, 1:M, 0:L-1])/hx +
                          (Vp[1:N, 1:M, 1:L] - Vp[1:N, 0:M-1, 1:L])/hy +
                          (Wp[1:N, 1:M, 1:L] - Wp[0:N-1, 1:M, 1:L])/hz)/dt
    Pp = multigrid(rhs, hx, hy, hz)

    # Add pressure correction.
    P = P + Pp

    # Update new values for U, V and W
    U[1:N, 1:M, 1:L-1] = Up[1:N, 1:M, 1:L-1] - dt*(Pp[1:N, 1:M, 2:L] - Pp[1:N, 1:M, 1:L-1])/hx
    V[1:N, 1:M-1, 1:L] = Vp[1:N, 1:M-1, 1:L] - dt*(Pp[1:N, 2:M, 1:L] - Pp[1:N, 1:M-1, 1:L])/hy
    W[1:N-1, 1:M, 1:L] = Wp[1:N-1, 1:M, 1:L] - dt*(Pp[2:N, 1:M, 1:L] - Pp[1:N-1, 1:M, 1:L])/hz

    # Calculating updated values of T implicitly
    Ht = np.zeros([N+1, M+1, L+1])
    Ht[1:N, 1:M, 1:L] = T[1:N, 1:M, 1:L] + dt*((DDXi(T, N, M, L, hx) + DDEt(T, N, M, L, hy) + DDZt(T, N, M, L, hz))*0.5 -
                      0.50*(U[1:N, 1:M, 0:L-1] + U[1:N, 1:M, 1:L])*D_Xi(T, N, M, L, hx) -
                      0.50*(V[1:N, 0:M-1, 1:L] + V[1:N, 1:M, 1:L])*D_Et(T, N, M, L, hy) - 
                      0.50*(W[0:N-1, 1:M, 1:L] + W[1:N, 1:M, 1:L])*D_Zt(T, N, M, L, hz))
    T = tJacobi(Ht, hx, hy, hz)

    # Impose BC on new values of U, V and W
    U = imposeUBCs(U)
    V = imposeVBCs(V)
    W = imposeWBCs(W)
    T = imposeTBCs(T)

    return U, V, W, P, T


def DDXi(inpFld, Nz, Ny, Nx, hx):
    outFld = np.zeros_like(inpFld)
    outFld[1:Nz, 1:Ny, 1:Nx] = (inpFld[1:Nz, 1:Ny, 0:Nx-1] - 2.0*inpFld[1:Nz, 1:Ny, 1:Nx] + inpFld[1:Nz, 1:Ny, 2:Nx+1])/(hx*hx)

    return outFld[1:Nz, 1:Ny, 1:Nx]


def DDEt(inpFld, Nz, Ny, Nx, hy):
    outFld = np.zeros_like(inpFld)
    outFld[1:Nz, 1:Ny, 1:Nx] = (inpFld[1:Nz, 0:Ny-1, 1:Nx] - 2.0*inpFld[1:Nz, 1:Ny, 1:Nx] + inpFld[1:Nz, 2:Ny+1, 1:Nx])/(hy*hy)

    return outFld[1:Nz, 1:Ny, 1:Nx]


def DDZt(inpFld, Nz, Ny, Nx, hz):
    outFld = np.zeros_like(inpFld)
    outFld[1:Nz, 1:Ny, 1:Nx] = (inpFld[0:Nz-1, 1:Ny, 1:Nx] - 2.0*inpFld[1:Nz, 1:Ny, 1:Nx] + inpFld[2:Nz+1, 1:Ny, 1:Nx])/(hz*hz)

    return outFld[1:Nz, 1:Ny, 1:Nx]


def D_Xi(inpFld, Nz, Ny, Nx, hx):
    outFld = np.zeros_like(inpFld)
    outFld[1:Nz, 1:Ny, 1:Nx] = (inpFld[1:Nz, 1:Ny, 2:Nx+1] - inpFld[1:Nz, 1:Ny, 0:Nx-1])*0.5/hx

    return outFld[1:Nz, 1:Ny, 1:Nx]


def D_Et(inpFld, Nz, Ny, Nx, hy):
    outFld = np.zeros_like(inpFld)
    outFld[1:Nz, 1:Ny, 1:Nx] = (inpFld[1:Nz, 2:Ny+1, 1:Nx] - inpFld[1:Nz, 0:Ny-1, 1:Nx])*0.5/hy

    return outFld[1:Nz, 1:Ny, 1:Nx]


def D_Zt(inpFld, Nz, Ny, Nx, hz):
    outFld = np.zeros_like(inpFld)
    outFld[1:Nz, 1:Ny, 1:Nx] = (inpFld[2:Nz+1, 1:Ny, 1:Nx] - inpFld[0:Nz-1, 1:Ny, 1:Nx])*0.5/hz

    return outFld[1:Nz, 1:Ny, 1:Nx]


# Velocity BCs for U - **Boundary walls at staggered point**
def imposeUBCs(U):
    global xyPeriodic

    # Periodic BCs along X and Y directions
    if xyPeriodic:
        # Left wall
        U[:, :, 0] = U[:, :, -2]

        # Right wall
        U[:, :, -1] = U[:, :, 1]

        # Front wall
        U[:, 0, :] = U[:, -3, :]

        # Back wall
        U[:, -1, :] = U[:, 2, :]

    # No-slip and no-penetration BCs
    else:
        # Left wall
        U[:, :, 0] = -U[:, :, 1]

        # Right wall
        U[:, :, -1] = -U[:, :, -2]

        # Front wall
        U[:, 0, :] = 0.0

        # Back wall
        U[:, -1, :] = 0.0

    # Bottom wall
    U[0, :, :] = 0.0

    # Top wall
    U[-1, :, :] = 0.0

    return U


# Velocity BCs for V - **Boundary walls at staggered point**
def imposeVBCs(V):
    global xyPeriodic

    # Periodic BCs along along X and Y directions
    if xyPeriodic:
        # Left wall
        V[:, :, 0] = V[:, :, -3]

        # Right wall
        V[:, :, -1] = V[:, :, 2]

        # Front wall
        V[:, 0, :] = V[:, -2, :]

        # Back wall
        V[:, -1, :] = V[:, 1, :]

    # No-slip and no-penetration BCs
    else:
        # Left wall
        V[:, :, 0] = 0.0

        # Right wall
        V[:, :, -1] = 0.0

        # Front wall
        V[:, 0, :] = -V[:, 1, :]

        # Back wall
        V[:, -1, :] = -V[:, -2, :]

    # Bottom wall
    V[0, :, :] = 0.0

    # Top wall
    V[-1, :, :] = 0.0

    return V


# Velocity BCs for W - **Boundary walls at staggered point**
def imposeWBCs(W):
    global xyPeriodic

    # Periodic BCs along X and Y directions
    if xyPeriodic:
        # Left wall
        W[:, :, 0] = W[:, :, -3]

        # Right wall
        W[:, :, -1] = W[:, :, 2]

        # Front wall
        W[:, 0, :] = W[:, -3, :]

        # Back wall
        W[:, -1, :] = W[:, 2, :]

    # No-slip and no-penetration BCs
    else:
        # Left wall
        W[:, :, 0] = 0.0

        # Right wall
        W[:, :, -1] = 0.0

        # Front wall
        W[:, 0, :] = 0.0

        # Back wall
        W[:, -1, :] = 0.0

    # Bottom wall
    W[0, :, :] = -W[1, :, :]

    # Top wall
    W[-1, :, :] = -W[-2, :, :]

    return W

 
# Temperature BCs for adiabatic no-slip walls - **Boundary walls at staggered point**
def imposeTBCs(T):
    global xyPeriodic

    # Periodic boundary conditions along X and Y directions
    if xyPeriodic:
        # Left wall
        T[:, :, 0] = T[:, :, -3]

        # Right wall
        T[:, :, -1] = T[:, :, 2]

        # Front wall
        T[:, 0, :] = T[:, -3, :]

        # Back wall
        T[:, -1, :] = T[:, 2, :]

    # Neumann boundary conditions
    else:
        #Left wall
        T[:, :, 0] = T[:, :, 1]
        
        #Right wall
        T[:, :, -1] = T[:, :, -2]
        
        #Front wall
        T[:, 0, :] = T[:, 1, :]
        
        #Back wall
        T[:, -1, :] = T[:, -2, :]
    
    #Bottom Wall - Hot wall - Is temerature or flux set here? Ask Shashwat **
    T[0, :, :] = 2 - T[1, :, :]
    
    #Top Wall - Cold wall - Is temerature or flux set here? Ask Shashwat **
    T[-1, :, :] = -T[-2, :, :] 
    
    return T


# Pressure BCs for P - **Boundary walls at staggered point**
def imposePBCs(P):
    global xyPeriodic

    # Periodic boundary conditions along X and Y directions
    if xyPeriodic:
        # Left wall
        P[:, :, 0] = P[:, :, -3]

        # Right wall
        P[:, :, -1] = P[:, :, 2]

        # Front wall
        P[:, 0, :] = P[:, -3, :]

        # Back wall
        P[:, -1, :] = P[:, 2, :]

    # Neumann boundary conditions
    else:
        # Left wall
        P[:, :, 0] = P[:, :, 2];        #P[:, :, 1] = P[:, :, 2]

        # Right wall
        P[:, :, -1] = P[:, :, -3];      #P[:, :, -2] = P[:, :, -3]

        # Front wall
        P[:, 0, :] = P[:, 2, :];        #P[:, 1, :] = P[:, 2, :]

        # Back wall
        P[:, -1, :] = P[:, -3, :];      #P[:, -2, :] = P[:, -3, :]

    # Bottom wall
    P[0, :, :] = P[2, :, :];        #P[1, :, :] = P[2, :, :]

    # Top wall
    P[-1, :, :] = P[-3, :, :];      #P[-2, :, :] = P[-3, :, :]

    return P


#Jacobi iterative solver for U
def uJacobi(rho, hx, hy, hz):
    global Pr, dt
    global N, M, L
    global tolerance
    global iCnt, opInt

    prev_sol = np.zeros_like(rho)
    next_sol = np.zeros_like(rho)
    test_sol = np.zeros_like(rho)
    jCnt = 0

    while True:
        next_sol[1:N, 1:M, 1:L-1] = (((hy*hy)*(hz*hz)*(prev_sol[1:N,   1:M, 0:L-2] + prev_sol[1:N,   1:M,   2:L]) +
                                      (hx*hx)*(hz*hz)*(prev_sol[1:N, 0:M-1, 1:L-1] + prev_sol[1:N, 2:M+1, 1:L-1]) +
                                      (hx*hx)*(hy*hy)*(prev_sol[0:N-1, 1:M, 1:L-1] + prev_sol[2:N+1, 1:M, 1:L-1]))*
                                       dt/((hx*hx)*(hy*hy)*(hz*hz)*2.0/Pr) + rho[1:N, 1:M, 1:L-1])/ \
                                (1.0 + dt*Pr*((hy*hy)*(hz*hz) + (hx*hx)*(hz*hz) + (hx*hx)*(hy*hy))/((hx*hx)*(hy*hy)*(hz*hz)))

        # IMPOSE BOUNDARY CONDITION AND COPY TO PREVIOUS SOLUTION ARRAY
        next_sol = imposeUBCs(next_sol)
        prev_sol = np.copy(next_sol)

        test_sol[1:N, 1:M, 1:L-1] = next_sol[1:N,   1:M, 1:L-1] - (
                                    DDXi(next_sol, N, M, L-1, hx) + DDEt(next_sol, N, M, L-1, hy) + DDZt(next_sol, N, M, L-1, hz))*0.5*dt*Pr

        error_temp = np.fabs(rho[1:N, 1:M, 1:L-1] - test_sol[1:N, 1:M, 1:L-1])
        maxErr = np.amax(error_temp)
        if maxErr < tolerance:
            if iCnt % opInt == 0:
                print "Jacobi solver for U converged in ", jCnt, " iterations"
            break

        jCnt += 1
        if jCnt > 10*N*M*L:
            print "ERROR: Jacobi not converging in U. Aborting"
            print "Maximum error: ", maxErr
            quit()

    return prev_sol


#Jacobi iterative solver for V
def vJacobi(rho, hx, hy, hz):
    global Pr, dt
    global N, M, L
    global tolerance
    global iCnt, opInt

    prev_sol = np.zeros_like(rho)
    next_sol = np.zeros_like(rho)
    test_sol = np.zeros_like(rho)
    jCnt = 0

    while True:
        next_sol[1:N, 1:M-1, 1:L] = (((hy*hy)*(hz*hz)*(prev_sol[1:N, 1:M-1, 0:L-1] + prev_sol[1:N, 1:M-1, 2:L+1]) +
                                      (hx*hx)*(hz*hz)*(prev_sol[1:N, 0:M-2,   1:L] + prev_sol[1:N, 2:M,     1:L]) +
                                      (hx*hx)*(hy*hy)*(prev_sol[0:N-1, 1:M-1, 1:L] + prev_sol[2:N+1, 1:M-1, 1:L]))*
                                       dt/((hx*hx)*(hy*hy)*(hz*hz)*2.0/Pr) + rho[1:N, 1:M-1, 1:L])/ \
                                (1.0 + dt*Pr*((hy*hy)*(hz*hz) + (hx*hx)*(hz*hz) + (hx*hx)*(hy*hy))/((hx*hx)*(hy*hy)*(hz*hz)))

        # IMPOSE BOUNDARY CONDITION AND COPY TO PREVIOUS SOLUTION ARRAY
        next_sol = imposeVBCs(next_sol)
        prev_sol = np.copy(next_sol)

        test_sol[1:N, 1:M-1, 1:L] = next_sol[1:N, 1:M-1,   1:L] - (
                                    DDXi(next_sol, N, M-1, L, hx) + DDEt(next_sol, N, M-1, L, hy) + DDZt(next_sol, N, M-1, L, hz))*0.5*dt*Pr

        error_temp = np.fabs(rho[1:N, 1:M-1, 1:L] - test_sol[1:N, 1:M-1, 1:L])
        maxErr = np.amax(error_temp)
        if maxErr < tolerance:
            if iCnt % opInt == 0:
                print "Jacobi solver for V converged in ", jCnt, " iterations"
            break

        jCnt += 1
        if jCnt > 10*N*M*L:
            print "ERROR: Jacobi not converging in V. Aborting"
            print "Maximum error: ", maxErr
            quit()

    return prev_sol


#Jacobi iterative solver for W
def wJacobi(rho, hx, hy, hz):
    global Pr, dt
    global N, M, L
    global tolerance
    global iCnt, opInt

    prev_sol = np.zeros_like(rho)
    next_sol = np.zeros_like(rho)
    test_sol = np.zeros_like(rho)
    jCnt = 0

    while True:
        next_sol[1:N-1, 1:M, 1:L] = (((hy*hy)*(hz*hz)*(prev_sol[1:N-1, 1:M, 0:L-1] + prev_sol[1:N-1, 1:M, 2:L+1]) +
                                      (hx*hx)*(hz*hz)*(prev_sol[1:N-1, 0:M-1, 1:L] + prev_sol[1:N-1, 2:M+1, 1:L]) +
                                      (hx*hx)*(hy*hy)*(prev_sol[0:N-2, 1:M,   1:L] + prev_sol[2:N,   1:M,   1:L]))*
                                       dt/((hx*hx)*(hy*hy)*(hz*hz)*2.0/Pr) + rho[1:N-1, 1:M, 1:L])/ \
                                (1.0 + dt*Pr*((hy*hy)*(hz*hz) + (hx*hx)*(hz*hz) + (hx*hx)*(hy*hy))/((hx*hx)*(hy*hy)*(hz*hz)))

        # IMPOSE BOUNDARY CONDITION AND COPY TO PREVIOUS SOLUTION ARRAY
        next_sol = imposeWBCs(next_sol)
        prev_sol = np.copy(next_sol)

        test_sol[1:N-1, 1:M, 1:L] = next_sol[1:N-1, 1:M,   1:L] - (
                                    DDXi(next_sol, N-1, M, L, hx) + DDEt(next_sol, N-1, M, L, hy) + DDZt(next_sol, N-1, M, L, hz))*0.5*dt*Pr

        error_temp = np.fabs(rho[1:N-1, 1:M, 1:L] - test_sol[1:N-1, 1:M, 1:L])
        maxErr = np.amax(error_temp)
        if maxErr < tolerance:
            if iCnt % opInt == 0:
                print "Jacobi solver for W converged in ", jCnt, " iterations"
            break

        jCnt += 1
        if jCnt > 10*N*M*L:
            print "ERROR: Jacobi not converging in W. Aborting"
            print "Maximum error: ", maxErr
            quit()

    return prev_sol


#Jacobi iterative solver for T
def tJacobi(rho, hx, hy, hz):
    global dt
    global N, M, L
    global tolerance
    global iCnt, opInt

    prev_sol = np.zeros_like(rho)
    next_sol = np.zeros_like(rho)
    test_sol = np.zeros_like(rho)
    jCnt = 0

    while True:
        next_sol[1:N, 1:M, 1:L] = (((hy*hy)*(hz*hz)*(prev_sol[1:N, 1:M, 0:L-1] + prev_sol[1:N, 1:M, 2:L+1]) +
                                    (hx*hx)*(hz*hz)*(prev_sol[1:N, 0:M-1, 1:L] + prev_sol[1:N, 2:M+1, 1:L]) +
                                    (hx*hx)*(hy*hy)*(prev_sol[0:N-1, 1:M, 1:L] + prev_sol[2:N+1, 1:M, 1:L]))*
                                dt/((hx*hx)*(hy*hy)*(hz*hz)*2.0) + rho[1:N, 1:M, 1:L])/ \
                                (1.0 + dt*((hy*hy)*(hz*hz) + (hx*hx)*(hz*hz) + (hx*hx)*(hy*hy))/((hx*hx)*(hy*hy)*(hz*hz)))

        # IMPOSE BOUNDARY CONDITION AND COPY TO PREVIOUS SOLUTION ARRAY
        next_sol = imposeTBCs(next_sol)
        prev_sol = np.copy(next_sol)

        test_sol[1:N, 1:M, 1:L] = next_sol[1:N, 1:M,   1:L] - (
                             DDXi(next_sol, N, M, L, hx) + DDEt(next_sol, N, M, L, hy) + DDZt(next_sol, N, M, L, hz))*0.5*dt

        error_temp = np.fabs(rho[1:N, 1:M, 1:L] - test_sol[1:N, 1:M, 1:L])
        maxErr = np.amax(error_temp)
        if maxErr < tolerance:
            if iCnt % opInt == 0:
                print "Jacobi solver for T converged in ", jCnt, " iterations"
            break

        jCnt += 1
        if jCnt > 10*N*M*L:
            print "ERROR: Jacobi not converging in T. Aborting"
            print "Maximum error: ", maxErr
            quit()

    return prev_sol


#Multigrid solver
def multigrid(H, hx, hy, hz):
    global N, M, L, vcCnt

    P = np.zeros([N+1, M+1, L+1])
    chMat = np.ones([N+1, M+1, L+1])
    erMat = np.zeros([N+1, M+1, L+1])
    for i in range(vcCnt):
        P = v_cycle(P, H, hx, hy, hz)
        chMat = laplace(P, hx, hy, hz)

    erMat = np.abs(H[1:N, 1:M, 1:L] - chMat[1:N, 1:M, 1:L])
    print "Error after multigrid is ", np.amax(erMat)

    return P


#Multigrid solution without the use of recursion
def v_cycle(P, H, hx, hy, hz):
    global sInd, VDepth
    global preSm, pstSm, proSm

    # Pre-smoothing
    P = smooth(P, H, hx, hy, hz, preSm, 0)

    H_rsdl = H - laplace(P, hx, hy, hz)

    # Restriction operations
    for i in range(VDepth):
        sInd -= 1
        H_rsdl = restrict(H_rsdl)

    # Solving the system after restriction
    P_corr = solve(H_rsdl, (2.0**VDepth)*hx, (2.0**VDepth)*hy, (2.0**VDepth)*hz)

    # Prolongation operations
    for i in range(VDepth):
        sInd += 1
        P_corr = prolong(P_corr)
        H_rsdl = prolong(H_rsdl)
        P_corr = smooth(P_corr, H_rsdl, hx, hy, hz, proSm, VDepth-i-1)

    P += P_corr

    # Post-smoothing
    P = smooth(P, H, hx, hy, hz, pstSm, 0)

    return P


#Uses jacobi iteration to smooth the solution passed to it.
def smooth(function, rho, hx, hy, hz, iteration_times, vLevel):
    smoothed = np.copy(function)

    # 1 subtracted from shape to account for ghost points
    [N, M, L] = np.array(np.shape(function)) - 1

    for i in range(iteration_times):
        toSmooth = imposePBCs(smoothed)

        smoothed[1:N, 1:M, 1:L] = (
            (hy*hy)*(hz*hz)*(toSmooth[1:N, 1:M, 2:L+1] + toSmooth[1:N, 1:M, 0:L-1]) +
            (hx*hx)*(hz*hz)*(toSmooth[1:N, 2:M+1, 1:L] + toSmooth[1:N, 0:M-1, 1:L]) +
            (hx*hx)*(hy*hy)*(toSmooth[2:N+1, 1:M, 1:L] + toSmooth[0:N-1, 1:M, 1:L]) -
            (hx*hx)*(hy*hy)*(hz*hz)*rho[1:N, 1:M, 1:L])/ \
      (2.0*((hy*hy)*(hz*hz) + (hx*hx)*(hz*hz) + (hx*hx)*(hy*hy)))

    return smoothed


#Reduces the size of the array to a lower level, 2^(n-1)+1.
def restrict(function):
    global sInd, sLst

    [rz, ry, rx] = [sLst[sInd[0]], sLst[sInd[1]], sLst[sInd[2]]]
    restricted = np.zeros([rz + 1, ry + 1, rx + 1])

    for i in range(1, rz):
        for j in range(1, ry):
            for k in range(1, rx):
                restricted[i, j, k] = function[2*i - 1, 2*j - 1, 2*k - 1]

    return restricted


#Increases the size of the array to a higher level, 2^(n+1)+1.
def prolong(function):
    global sInd, sLst

    [rz, ry, rx] = [sLst[sInd[0]], sLst[sInd[1]], sLst[sInd[2]]]
    prolonged = np.zeros([rz + 1, ry + 1, rx + 1])

    [lz, ly, lx] = np.shape(function)
    for i in range(1, lz-1):
        for j in range(1, ly-1):
            for k in range(1, lx-1):
                prolonged[i*2 - 1, j*2 - 1, k*2 - 1] = function[i, j, k]
    
    for i in range(1, rz, 2):
        for j in range(1, ry, 2):
            for k in range(2, rx, 2):
                prolonged[i, j, k] = (prolonged[i, j, k-1] + prolonged[i, j, k+1])/2

    for i in range(1, rz, 2):
        for j in range(2, ry, 2):
            for k in range(1, rx):
                prolonged[i, j, k] = (prolonged[i, j-1, k] + prolonged[i, j+1, k])/2

    for i in range(2, rz, 2):
        for j in range(1, ry):
            for k in range(1, rx):
                prolonged[i, j, k] = (prolonged[i-1, j, k] + prolonged[i+1, j, k])/2

    return prolonged


#This function uses the Jacobi iterative solver, using the grid spacing
def solve(rho, hx, hy, hz):
    global tolerance

    # 1 subtracted from shape to account for ghost points
    [N, M, L] = np.array(np.shape(rho)) - 1
    prev_sol = np.zeros_like(rho)
    next_sol = np.zeros_like(rho)
    jCnt = 0

    while True:
        next_sol[1:N, 1:M, 1:L] = (
            (hy*hy)*(hz*hz)*(prev_sol[1:N, 1:M, 2:L+1] + prev_sol[1:N, 1:M, 0:L-1]) +
            (hx*hx)*(hz*hz)*(prev_sol[1:N, 2:M+1, 1:L] + prev_sol[1:N, 0:M-1, 1:L]) +
            (hx*hx)*(hy*hy)*(prev_sol[2:N+1, 1:M, 1:L] + prev_sol[0:N-1, 1:M, 1:L]) -
            (hx*hx)*(hy*hy)*(hz*hz)*rho[1:N, 1:M, 1:L])/ \
      (2.0*((hy*hy)*(hz*hz) + (hx*hx)*(hz*hz) + (hx*hx)*(hy*hy)))

        solLap = np.zeros_like(next_sol)
        solLap[1:N, 1:M, 1:L] = DDXi(next_sol, N, M, L, hx) + DDEt(next_sol, N, M, L, hy) + DDZt(next_sol, N, M, L, hz)

        error_temp = np.abs(rho[1:N, 1:M, 1:L] - solLap[1:N, 1:M, 1:L])
        maxErr = np.amax(error_temp)
        if maxErr < tolerance:
            break

        jCnt += 1
        if jCnt > 10*N*M*L:
            print "ERROR: Jacobi not converging. Aborting"
            print "Maximum error: ", maxErr
            quit()

        prev_sol = np.copy(next_sol)

    return prev_sol


def laplace(function, hx, hy, hz):
    '''
Function to calculate the Laplacian for a given field of values.
INPUT:  function: 3D matrix of double precision values
        hx, hy, hz: Grid spacing of the uniform grid along x, y and z directions
OUTPUT: gradient: 3D matrix of double precision values with same size as input matrix
    '''

    # 1 subtracted from shape to account for ghost points
    [N, M, L] = np.array(np.shape(function)) - 1
    gradient = np.zeros_like(function)

    gradient[1:N, 1:M, 1:L] = DDXi(function, N, M, L, hx) + DDEt(function, N, M, L, hy) + DDZt(function, N, M, L, hz)

    return gradient


def getDiv(U, V, W):
    '''
Function to calculate the divergence within the domain (excluding walls)
INPUT:  U, V, W: Velocity values
OUTPUT: The maximum value of divergence in double precision
    '''
    global N, M, L
    global xColl, yColl, zColl

    divMat = np.zeros([N+1, M+1, L+1])
    for i in range(1, N):
        for j in range(1, M):
            for k in range(1, L):
                divMat[i, j, k] = (U[i, j, k] - U[i, j, k-1])/(xColl[k] - xColl[k-1]) + \
                                  (V[i, j, k] - V[i, j-1, k])/(yColl[j] - yColl[j-1]) + \
                                  (W[i, j, k] - W[i-1, j, k])/(zColl[i] - zColl[i-1])

    return np.unravel_index(divMat.argmax(), divMat.shape), np.amax(divMat)


def writeSoln(U, V, W, P, T, time):
    fName = "Soln_" + "{0:09.5f}.h5".format(time)

    print "Writing solution file: ", fName

    f = hp.File(fName, "w")

    dset = f.create_dataset("U",data = U)
    dset = f.create_dataset("V",data = V)
    dset = f.create_dataset("W",data = W)
    dset = f.create_dataset("P",data = P)
    dset = f.create_dataset("T",data = T)

    f.close()


def calculateMetrics(hx, hy, hz):
    global xColl, yColl, zColl
    global xStag, yStag, zStag

    xi = np.linspace(0.0, 1.0, L)
    et = np.linspace(0.0, 1.0, M)
    zt = np.linspace(0.0, 1.0, N)

    # Calculate grid and its metrics
    xColl = np.array([i for i in xi])
    yColl = np.array([i for i in et])
    zColl = np.array([i for i in zt])

    xStag = np.array([(xi[j] + xi[j+1])/2 for j in range(len(xi) - 1)])
    yStag = np.array([(et[j] + et[j+1])/2 for j in range(len(et) - 1)])
    zStag = np.array([(zt[j] + zt[j+1])/2 for j in range(len(zt) - 1)])


# Main segment of code.
def main():
    global dt, tMax, fwInt, opInt
    global time, iCnt
    global N, M, L

    # Create and initialize U, V, W and P arrays
    # The arrays have two extra points
    # These act as ghost points on either sides of the domain
    P = np.ones([N+1, M+1, L+1])

    # Temeprature field is evaluated at the same points as the pressure
    T = np.ones([N+1, M+1, L+1])

    # U is staggered in Y and Z directions and hence has one extra point along these directions
    U = np.zeros([N+1, M+1, L])

    # V is staggered in X and Z directions and hence has one extra point along these directions
    V = np.zeros([N+1, M, L+1])

    # W is staggered in X and Y directions and hence has one extra point along these directions
    W = np.zeros([N, M+1, L+1])

    # Define grid spacings
    hx = 1.0/(L-1)
    hy = 1.0/(M-1)
    hz = 1.0/(N-1)

    calculateMetrics(hx, hy, hz)

    # Initial conditions for Rayleigh-Benard Convection #
    # Linearly varying mean temperature profile is imposed
    print "Computing Rayleigh-Benard Convection on {0:3d} x{1:3d} x{2:3d} uniform grid".format(N, M, L)
    for z in range(N+1):
        T[z,:,:] = 1.0 - float(z)/N

    fwTime = 0.0

    while True:
        if abs(fwTime - time) < 0.5*dt:
            writeSoln(U, V, W, P, T, time)
            fwTime += fwInt

        U, V, W, P, T = euler(U, V, W, P, T, hx, hy, hz)

        maxDiv = getDiv(U, V, W)
        if maxDiv[1] > 10.0:
            print "ERROR: Divergence has exceeded permissible limits. Aborting"
            quit()

        iCnt += 1
        time += dt
        if iCnt % opInt == 0:
            print "Time: {0:9.5f}".format(time)
            print "Maximum divergence: {0:8.5f} at ({1:d}, {2:d}, {3:d})\n".format(maxDiv[1], maxDiv[0][0], maxDiv[0][1], maxDiv[0][2])

        if time > tMax:
            break

    print "Simulation completed"


###########################################################################################################################################

# N should be of the form 2^n+2 so that staggered pressure points will be 2^n + 3 including ghost points
sLst = [2**x + 2 for x in range(12)]

# Choose the grid sizes from sLst so as to allow for Multigrid operations
# [2, 4, 6, 10, 18, 34, 66, 130, 258, 514, 1026, 2050]
#  0  1  2  3   4   5   6    7    8    9    10    11
sInd = np.array([5, 5, 5])
N = sLst[sInd[0]]
M = sLst[sInd[1]]
L = sLst[sInd[2]]

xyPeriodic = True

# Limits along each direction
# L - Along X
# M - Along Y
# N - Along Z
# Data stored in arrays accessed by data[1:N, 1:M, 1:L]
# In Python and C, the rightmost index varies fastest
# Therefore indices in X direction vary fastest, then along Y and finally along Z

# Grid metric arrays. Used by the enitre program at various points
xColl = np.zeros(L)
yColl = np.zeros(M)
zColl = np.zeros(N)
xStag = np.zeros(L-1)
yStag = np.zeros(M-1)
zStag = np.zeros(N-1)

# Tolerance value in Jacobi iterations
tolerance = 0.00001

# Depth of each V-cycle in multigrid
VDepth = 3

# Number of V-cycles to be computed
vcCnt = 10

# Number of iterations during pre-smoothing
preSm = 10

# Number of iterations during post-smoothing
pstSm = 40

# Number of iterations during smoothing in between prolongation operators
proSm = 20

time = 0.0
iCnt = 0

dt = 0.01
opInt = 1
fwInt = 0.5
tMax = 10.0
Pr = 1
Ra = 1500

main()
